#!/usr/bin/python3
import argparse
import numpy as np
import pylab as pl

def plot_wp(data, ax):
    t = data.T[1]
    x = data.T[3]
    width = data.T[9]
    ax.plot(t, x, label="X")
    ax.plot(t, width, label="Width")
    print(data[30])
    return ax

def main():
    parser = argparse.ArgumentParser("Plots wp parameters")
    parser.add_argument("wp_file", help="wp.dat to plot")
    parser.add_argument("-o", "--output-file", help="file to save the plot")
    args = parser.parse_args()
    data = np.genfromtxt(args.wp_file)
    fig, ax = pl.subplots()
    plot_wp(data, ax)
    ax.set_xlabel("t")
    ax.set_ylabel("x")
    ax.legend()
    if args.output_file:
        fig.savefig(args.output_file, bbox="tight")
    else:
        pl.show()

if __name__ == '__main__':
    main()
