from __future__ import division
import numpy as np
import pylab as pl
import os
import sys
from numpy import sqrt, log
import re
import cStringIO
from configobj import ConfigObj
import os
import re

# Site specific data
epsilon_0 = 8.85418781762e-12
c = 299792458
MethodsNames={"Epsilons":"VTS_Improved", "Simple":"VTS_Steven", "Predictive":"VTS_Predictive", "None":"VTS_None"}

class Serie:
  def set_data(self, data):
    self.data = data
    self.k = self.data[0]
    self.t = self.data[1]
    self.E = self.data[2]
    self.dt = self.data[3]
    self.dE = self.data[4]
    self.recoil = self.data[5]
    self.nconstr = self.data[6]

  def __init__(self, filename, get_totals = False):
    infile = open(filename)
    cfg_str = infile.read()
    infile.close()
    cfg_str = re.sub(r'\$\((?P<var>\w+)\)', r'%($\g<var>)s', cfg_str)
    cfg_str = re.sub(r';', r'#', cfg_str)
    cfg_str = cStringIO.StringIO(cfg_str)
    self.config = ConfigObj(cfg_str)
    # Just fancy printing
    if 'Laser' in self.config.keys():
      self.I = E2I(self.config['Laser'].as_float('Amplitude'))

    self.VTS_Method = MethodsNames[self.config['MD']['VtsMethod']]
    try:
      vts_filename = os.path.join(os.path.relpath(os.path.split(filename)[0]+
                                       self.config['MD']['OutputVTS']))
      print(vts_filename)
    except KeyError:
      vts_filename = None

    self.totals = None
    if get_totals:
      tot_filename = os.path.relpath(os.path.split(filename)[0]+
                                   self.config.get('MD').get('OutputTotals'))
      print(tot_filename)
      self.totals = np.genfromtxt(tot_filename, invalid_raise=False).T
      self.ln_det = self.totals[3]

    if vts_filename is not None:
      self.set_data(np.genfromtxt(vts_filename, invalid_raise = False).T)
  def __getitem__(self, i):
    return self.data[i]

  def __iter__(self):
    return self.data

  def __len__(self):
    return len(self.k)

def sf(filename, figure=None):
  if figure is None:
    figure=pl.gcf()
  figure.set_size_inches(10*2.3, 6*2.3)
  figure.savefig("pics/"+filename, bbox_inches="tight")

def getPar(name, par):
  return map(lambda param: re.search("(?<={0}_).+?(?=(_|(\.[a-z]+)))".format(param), name), par)

def E2I(E):
  return (E*5.14221e11)**2/2/1e4/1e15*c*epsilon_0

def I2E(I):
  # I was in W/cm^2 e-15
  E = np.sqrt(2*I*1e4*1e15/epsilon_0/c)
  E /= 5.14221e11
  return E
  # E is now in a. u.

def subst_cfg(line, cfg):
  """Substitutes in a line, syntaxis $(VAR), taking values from cfg"""
  return re.sub(r'\$\((?P<var>\w+)\)',
                lambda var: cfg['$'+var.groups()[0]],
                line)
