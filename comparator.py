#!/usr/bin/python2.7

# The script to compare two data files
# the files are assumed to be lines of space separated floats
# Usage:
# comparator.py file1.dat file1_vts.dat file2.dat \
#               file2_vts.dat lines_to_compare.dat
# file1.dat, file2.dat - files to compare
# file1_vts.dat, file2_vts.dat - files with t in second column
# Needed because to comparison requires points equal ts
# lines_to_compare.dat - a file with a single line of space-separated
# ints, which denote columns to compare in the original files
# column numbering starts from 0
# OR
# comparator.py file1.dat file1_vts.dat file2.dat file2_vts.dat n1 n2 n3 ... nN
# n1 ... nN - column numbers, numbering starts from 0
# Output:
# plot of max of abs normed difference between numbers in the columns from t

from __future__ import division
import argparse
import sys
import numpy as np
from scipy import interpolate
import pylab as pl
from itertools import izip


def pairwise(iterable):
    "s -> (s0,s1), (s2,s3), (s4, s5), ..."
    a = iter(iterable)
    return izip(a, a)


def convert_arg_line_to_args(arg_line):
    for arg in arg_line.split():
        if not arg.strip():
            continue
        yield arg


parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.convert_arg_line_to_args = convert_arg_line_to_args
parser.add_argument("-i", "--input-files", help="data and times input files",
                   nargs='+', required=True)
parser.add_argument("-n", "--columns-indexes", nargs='+', required=True,
                    help="columns to compare. Indexing starts from 0",
                    type=int)
parser.add_argument("-N", "--number-of-points", type=int,
                    help="number of time points to compare at")
parser.add_argument("-k", "--use-step-numbers", action="store_true",
                    help="get step numbers from the 0th column "
                    "in the data file")
args = parser.parse_args()

columns = args.columns_indexes
N = args.number_of_points
data = list()

# interpolator = lambda x,y: interpolate.interp1d(x,y, kind='linear',
# copy=False)
interpolator = interpolate.InterpolatedUnivariateSpline
for file_pair in pairwise(args.input_files):
    cur_t = np.genfromtxt(file_pair[1], usecols=[1, ],
                          invalid_raise=False)

    if args.use_step_numbers:
        raw_pars = np.genfromtxt(file_pair[0], usecols=[0, ] + columns,
                                 invalid_raise=False).T
        k_pars = np.array(raw_pars[0], dtype=int)
        # The lines in time file are still assumed
        # to correspond to step numbers
        # and steps in the data file to increase
        # monotonously
        max_step_t = len(cur_t)
        # max_index is needed because files can be damaged
        # so, not every time would get recorded
        max_index = np.searchsorted(raw_pars[0], max_step_t)
        cur_t = cur_t[k_pars[:max_index]]
        cur_pars = raw_pars[1:].T

    else:
        cur_pars = np.genfromtxt(file_pair[0], usecols=columns,
                                 invalid_raise=False)

    if len(cur_pars.shape) == 1:
        cur_pars = np.array([cur_pars, ]).T

    # Dropping recoils
    # Idea: a recoiled condition got next time smaller
    # TODO(kazeevn) optimize
    good = False
    while not good:
        valid = np.nonzero(map(lambda x: 0 if x <= 0 else 1,
                               np.diff(cur_t)))[0]
        if len(valid) == 0:
            print("Your files appear not to contain valid time sequence.")
            sys.exit(3)
        if len(cur_t) == len(valid) + 1:
            good = True
        cur_t = cur_t[valid]

    max_len = min(len(cur_t), len(cur_pars))
    cur_pars = cur_pars[valid[:max_len]].T
    cur_t = cur_t[valid[:max_len]]
    splines = map(lambda col: interpolator(cur_t, col,), cur_pars)
    data.append([file_pair[0], cur_t, cur_pars, splines])
    # data entry format:
    # filename, t, data columns, splnes.

if len(data) < 2:
    print("To compare you need to specify at least two datasets.")
    exit(1)

if N is None:
    N = min(map(lambda entry: len(entry[1]), data))
t_min = max(map(lambda entry: min(entry[1]), data))
t_max = min(map(lambda entry: max(entry[1]), data))
ts = np.linspace(t_min, t_max, num=N)
norms = map(lambda col:
            reduce(lambda a, b: max(a, *abs(b[2][col])), data, 0),
            range(len(columns)))
# TODO(kazeevn) test is that's really the maximum


def norm(t):
    # WARNING(kazeevn)
    # norm uses columns and data from outer scope
    cur_norm = 0
    what = None
    for col in xrange(len(columns)):
        col_splines = map(lambda dat: dat[3][col], data)
        pars = map(lambda f: f(t), col_splines)
        col_norm = abs(max(pars) - min(pars)) / norms[col]
        if col_norm > cur_norm:
            cur_norm = col_norm
            what = col
    return cur_norm, what

diff = np.array(map(norm, ts)).T
# TODO(kazeevn) make numpy recognize diff[1] as int

fig, ax = pl.subplots()
ax.plot(ts, diff[0])
pl.xlabel("t")
pl.ylabel("Normed max difference, 1")
ax_what = ax.twinx()
ax_what.plot(ts, map(lambda x: (columns[int(x)] if x is not None else None),
                     diff[1]), 'r', lw=4)
pl.ylabel("Determening column")
pl.grid()
pl.show()
