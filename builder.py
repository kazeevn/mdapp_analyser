#!/usr/bin/python

import sys
import re
import copy
from xml.dom.minidom import parse

class Particle:
    def __init__(self, origin = None):
        if origin is None:
            self.packets = []
            self.type = None
            self.mass = None
        else:
            self.packets = []
            for packet in origin.packets:
                self.packets.append(Packet(packet))
            self.type = copy.copy(origin.type)
            self.mass = copy.copy(origin.mass)

    def add_packet(self, packet):
        self.packets.append(packet)

    def move(self, coords, absolute=False):
        # coords is a [x,y,z] list
        for packet in self.packets:
            for coord in range(2):
                if absolute:
                    packet.params[coord] = coords[coord]
                else:
                    packet.params[coord] += coords[coord]

class Packet:
    def __init__(self, params):
        if hasattr(params, 'params'):
            self.params = copy.copy(params.params) # We are copying
        else:
            self.params = params             # Params are just params

        # It's unfortunate namedtuples are imutable
        self.x = self.params[0]
        self.y = self.params[1]
        self.z = self.params[2]
        self.width = self.params[3]
        self.particle = self.params[4]
        self.c1 = self.params[5]
        self.c2 = self.params[6]

def istr(num):
    """Checks if number is integer, it it's the case, returns
    string without the point"""
    if int(num) == num:
        return str(int(num))
    else:
        return str(num)

class System:
    def __init__(self, source=None):
        # particle contains packets
        # packets hold parameters

        if isinstance(source, str):
            self.init_from_file(source)
        elif isinstance(source, System):
            self.init_copy(source)
        elif source is None:
            self.particles = list()

    def init_copy(self, source):
        self.particles = dict()
        for part_id, particle in source.particles.iteritems():
            self.particles[part_id] = Particle(particle)
        self.XMLstring = source.XMLstring

    def init_from_file(self, filename):
        get_float = re.compile(r'[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?')

        self.XMLstring = parse(filename)
        self.particles = dict()
        particle_by_packet = list()

        for position_node in self.XMLstring.getElementsByTagName("position"):
            for line in position_node.lastChild.data.splitlines():
                line = line.split()
                if len(line) == 7:
                    packet = Packet(map(lambda x:float(
                        get_float.match(x).group()), line))
                    if packet.particle not in self.particles.keys():
                        self.particles[packet.particle] = Particle()

                    self.particles[packet.particle].add_packet(packet)
                    particle_by_packet.append(packet.particle)

        for mass_node in self.XMLstring.getElementsByTagName("mass"):
            packet = 0
            for line in mass_node.lastChild.data.splitlines():
                if len(line) > 0:
                    self.particles[particle_by_packet[packet]].mass = float(
                        get_float.match(line).group())
                    packet += 1

        for type_node in self.XMLstring.getElementsByTagName("type"):
            packet = 0
            for line in type_node.lastChild.data.splitlines():
                if len(line) > 0:
                    self.particles[particle_by_packet[packet]].type = line
                    packet += 1

        newpart = dict()
        for part_id, particle in self.particles.iteritems():
            if particle.type == 'i' and len(particle.packets) > 1:
                sys.stderr.write(
                    "Ion %d has multiple packets. Made more ions.\n" % part_id)
                min_id = min(self.particles.keys())
                for i, packet in enumerate(particle.packets[1:]):
                    newpart[min_id - 1 - i] = Particle()
                    newpart[min_id - 1 - i].packets.append(packet)
                    newpart[min_id - 1 - i].type = 'i'
                    newpart[min_id - 1 - i].mass = particle.mass
                particle.packets = [particle.packets[0],]
        self.particles.update(newpart)

    def write_particles(self):
        # Forst, check packets to belong to their particle
        for par_id in self.particles.keys():
            for packet in self.particles[par_id].packets:
                packet.params[4] = par_id
        # Then, we use format them according to empirical rule:
        #1 Ions have negative part_ids
        #2 Ions are written first
        ions = filter(lambda particle: particle.type=='i',
                      self.particles.itervalues())
        electrones = filter(lambda particle: particle.type=='e',
                            self.particles.itervalues())
        for i in range(len(ions)):
            for packet in ions[i].packets:
                packet.params[4] = -i - 1
        for e in range(len(electrones)):
            for packet in electrones[e].packets:
                packet.params[4] = e

        # Then, write the parameters
        self.XMLstring.getElementsByTagName("position")[0].lastChild.data = ''
        self.XMLstring.getElementsByTagName("velocity")[0].lastChild.data = ''
        self.XMLstring.getElementsByTagName("mass")[0].lastChild.data = ''
        self.XMLstring.getElementsByTagName("type")[0].lastChild.data = ''
        try:
            # Configuration recrods for charge don't have to exist
            self.XMLstring.getElementsByTagName("charge")[0].lastChild.data = ''
        except IndexError:
            pass

        for data in (ions, electrones):
            self.XMLstring.getElementsByTagName("position")[0].lastChild.data += reduce(
                lambda X, Y: X + reduce(lambda x, y: x + " " + istr(y), Y, "") + "\n", reduce(
                    lambda XX, part: XX + map(lambda XXX: XXX.params, part.packets),
                    data, []), "")
            self.XMLstring.getElementsByTagName("velocity")[0].lastChild.data += "0 0 0 0\n" * reduce(
                lambda X, part: X + len(part.packets), data, 0)
            self.XMLstring.getElementsByTagName("mass")[0].lastChild.data += reduce(
                lambda X, part: X + (str(part.mass)+"\n")*len(part.packets),
                data, '')
            self.XMLstring.getElementsByTagName("type")[0].lastChild.data += reduce(
                lambda X, part: X + (part.type+"\n")*len(part.packets),
                data, '')
            try:
                # Configuration recrods for charge don't have to exist
                self.XMLstring.getElementsByTagName("charge")[0].lastChild.data += reduce(
                    lambda X, part: X + (("1" if part.type == 'i' else "-1") +"\n")*len(part.packets),
                    data, '')
            except IndexError:
                pass

        return self.XMLstring
