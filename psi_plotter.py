#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
sys.path.append("/home/kna/mipt/mol_mod/mdapp_analyser")

import pylab as pl
import numpy as np
import argparse
from itertools import cycle
import matplotlib.animation as animation

from serie import getPar

def make_artist(axe, filename, normalize, *args,  **kwargs):
    ion_position = float(getPar(filename, 'l')[0].group())
    data = np.genfromtxt(filename)
    data[0] -= ion_position/2.0
    if normalize:
        integr = sum(data[1][:-1]*np.diff(data[0]))
        data[1] /= integr
    else:
        integr = sum(data[1][:-1]*np.diff(data[0]))
        print("Norm: %g" % integr)
    left_vline = axe.axvline(-ion_position/2.0, c='r')
    right_vline = axe.axvline(ion_position/2.0, c='r')
    das_plot = axe.plot(data[0], data[1], *args, **kwargs)[0]
#    label = axe.text(1, 1.2, ur"Distance {:3.2f} Å".format(ion_position))
    return (left_vline, right_vline, das_plot)#, label)


def draw_separate(filenames, artist):
    if len(filenames) != 2:
        print "Oops, only 2 is supported"
        sys.exit(1)
    axes = [0,0]
    fig, axes = pl.subplots(1, 2, sharey=True, figsize=(6, 3), dpi=400)
    labels = cycle((ur"$r=2.00\AA$", ur"$r=0.74\AA$"))
    axlabels = list()
    artists = list()
    for ax, filename, label in zip(axes, args.filenames, labels):
        axlabels.append(ax.set_xlabel(r'$x, \AA$'))
        axlabels.append(ax.set_ylabel(r'$\psi^2, 1$'))
        artists.append(artist(ax, filename, 'b', label=label))
        ax.legend()
        ax.set_xbound(upper=3)

    fig.savefig("pics/psi_plot.png", bbox_extra_artists=axlabels,
                bbox_inches='tight', dpi=400)
    return fig

def draw_animate(filenames, artist):
    fig, axe = pl.subplots(figsize=(8, 6.15), dpi=400)
    artists = list()
    ion_positions = list()
    for filename in filenames:
        artists.append(artist(axe, filename, 'b'))
        ion_positions.append(float(getPar(filename, 'l')[0].group()))
    ion_positions = np.array(ion_positions)
    order = ion_positions.argsort()
    artists = [artists[i] for i in order]
    ani = animation.ArtistAnimation(fig, artists, interval = 200)
    axe.set_xlim((-3, 3))
    axe.set_xlabel(r'$x, \AA$')
    axe.set_ylabel(r'$\psi^2, 1$')
    ani.save("pics/psi_plot.mp4", fps=10)
    return fig, ani

def draw_simple(filenames, artist):
    fig = pl.figure(figsize=(5, 4), dpi=400)
    ax = fig.add_subplot(111)
    for filename in filenames:
        artist(ax, filename)
    return fig

parser = argparse.ArgumentParser(
    description='Plots $\int dy dz \psi(x,y,z)')
parser.add_argument("-n", "--normalize", action="store_true",
                    help="Normalize the psi function")
parser.add_argument("filenames", type=str, nargs='+',
                    help='State files to plot')
style = parser.add_mutually_exclusive_group()
style.add_argument("-s", "--separate", action="store_true",
                    help="Plot each wavefunction separatly")
style.add_argument("-a", "--animate", action="store_true",
                    help="Draw animation")
parser.add_argument('--save-format', action='store', type=str,
                    default='pdf', help='save file format')

args = parser.parse_args()
count = len(args.filenames)

artist = lambda axe, filename, *args_, **kwargs_:\
    make_artist(axe, filename, args.normalize, *args_, **kwargs_)

if args.separate:
    fig = draw_separate(args.filenames, artist)
elif args.animate:
    fig, ani = draw_animate(args.filenames, artist)
else:
    fig = draw_simple(args.filename, artist)


