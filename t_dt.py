from __future__ import division
import numpy as np
import matplotlib as mpl
import pylab as pl
import sys
sys.path.append("/home/kna/mipt/mol_mod/mdapp_analyser")
from numpy import sqrt, log
import re
from serie import Serie, sf
import random

id = random.random()
data = map(Serie, filter(lambda x: x.find('.cfg')>0, sys.argv[1:]))

tE, subplt = pl.subplots()
pl.ylabel("time step")
ax = subplt.twinx()
pl.ylabel("nconstr")
for serie in data:
  x = serie.k
  subplt.plot(x, serie.dt, label=serie.config['Charm']['Legend'] +
              " N = %d, NConstrMax = %s"%
              (serie.k[-1], serie.config['MD']['MaxConstr']))
  subplt.scatter(x, serie.dt, s = 20 +30*serie.nconstr, c=serie.recoil)
  subplt.hlines(float(serie.config['MD']['TimeStepConstr'])*0.1, min(x), max(x))
  ax.plot(x, serie.nconstr, 'r', lw=3)

  #  traj = np.array(filter(lambda x: x[5] == 1, serie.data.T)).T
  # if len(traj)>0:
  #  subplt.scatter(traj[0], traj[3], label=serie.VTS_Method+"_N_%d - recoils"%serie.k[-1])

  interval=float(serie.config['MD']['LogInterval']) * 9.7871500348893013e-02
  if interval > 0:
    log_outputs = np.arange(0, serie.t[-1]+interval, interval)
    pl.vlines(log_outputs, min(serie.dt), max(serie.dt), 'r')

subplt.axes.set_yscale('log')
subplt.autoscale_view(True,True,True)

pl.xlabel("t")

subplt.legend(loc=4)
sf("t_dt_%f.png"%id, tE)
