from __future__ import division
import numpy as np
import pylab as pl
import sys
sys.path.append("/home/kna/mipt/mol_mod/mdapp_analyser")
from numpy import sqrt, log
import re
from serie import Serie, sf
import serie
import random

id = random.random()
data = map(Serie, filter(lambda x: x.find('.cfg')>0, sys.argv[1:]))

tE, ax = pl.subplots()
pl.ylabel("nconstr")
ax_E = ax.twinx()
pl.ylabel("E")
for serie in data:
  x = serie.k
  ax.plot(x, serie.nconstr, 'r', lw=3)
  E0 = serie.E[0]
  # ax.plot(x, serie.E-E0, label=serie.config['$NAME']+"_N_%d" % serie.k[-1])
  traj = np.array(filter(lambda x: x[5] == 0, serie.data.T)).T
  if len(traj)>0:
    ax_E.plot(traj[0], traj[2]-E0, label=serie.VTS_Method+"_NConst_%s"%
              serie.config['MD']['TimeStepConstr'])


# for axe in tE.axes:
#  axe.set_yscale('log')

pl.xlabel("t")
pl.legend(loc=4)
#pl.ylim([-20,0])
sf("tE_%f.png"%id, tE)
