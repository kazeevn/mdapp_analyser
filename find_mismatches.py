#!/usr/bin/python2.7

import numpy as np
import sys

k = np.genfromtxt(sys.argv[1], usecols=[0, ])
lines = np.nonzero(np.diff(k)-1)
print map(lambda x: k[x], lines)

