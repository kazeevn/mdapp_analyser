#!/usr/bin/python2.6

from configobj import ConfigObj
import random

s = 5
def_cfg = ConfigObj()
# K100
def_cfg["$BASE"] = "/home/imorozov/programs/source_tree_ionize/mdapp/ionization"
def_cfg["$NAME"] = "H1s_s{0}".format(s)
def_cfg["$SUFFIX"] = random.random()
def_cfg["$STATESDIR"] =  "$(BASE)/init_states"
def_cfg["$OUTDIR"]= "results"
def_cfg["Description"]= "Laser pulse"
MD={"TimeStep":1e-4,
    "MaxTime":5,
    "Method": "RK4",
    "VtsMethod": "Predictive",
    "TimeStepConstr": 1e-7,
    "Alpha": 1.1,
    "Beta": 0.5,
    "EpsilonH": 0.01,
    "Epsilon": 0.0035,
    "B": 5.4,
    "Algorithm": "MD",
    "LogInterval": 1e-3,
    "DisplayFreq": 100,
    "NormMode": "Normalize",
    "InputState": "$(STATESDIR)/$(NAME).xml",
    "OutputTotals": "$(OUTDIR)/$(NAME)$(SUFFIX)_tot.dat",
    "OutputPerWP": "/dev/null",
    "OutputSummary": "$(OUTDIR)/summary.dat",
    "OutputVTS": "$(OUTDIR)/$(NAME)$(SUFFIX)_vts.dat",
    "MinTimeStep": 1e-8,
    "PsiDumpInterval": 0.0,
    "MaxConstr": 2}
def_cfg['MD']=MD
Laser = {"Amplitude": 0.238723663464,  #I = 2.0
    "LaserFreq": 0.0569569142665,
    "HalfPulseLength": 219.098
    }
def_cfg['Laser']=Laser
Debug = {"Debug":"True"}
def_cfg['Debug']=Debug

# You want to change
# $SUFFIX
