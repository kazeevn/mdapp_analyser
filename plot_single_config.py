#!/usr/bin/python
import sys
sys.path.append("/home/kna/mipt/mol_mod/mdapp_analyser")

import pylab as pl
import numpy as np
import argparse
import itertools
import matplotlib as mpl
from serie import Serie, sf, getPar
from builder import System, Particle
from itertools import imap

parser = argparse.ArgumentParser(description='Plots wave packet state')
parser.add_argument('filename', type=str, nargs=1,
                    help='state.xml to plot')
parser.add_argument('-d', '--dimensions', type=int, nargs='*',
                    help="figure dimension, inches")
parser.add_argument('--save-format', action='store', type=str,
                    default='pdf', help='save file format')

args = parser.parse_args()
fig_params = dict()
if args.dimensions is not None:
    if len(args.dimensions) != 2:
        print("You maust specify both dimensions")
        sys.exit(1)
    fig_params['figsize'] = args.dimensions

system = System(args.filename[0])
ion_position = float(getPar(args.filename[0], 'l')[0].group())
electron = filter(lambda particle: particle.type=='e',
                  system.particles.itervalues())
packets =  np.transpose((np.array(map(lambda packet: (packet.x, packet.width, packet.c1),
                         electron[0].packets+electron[1].packets))), axes=(1,0))

fig_state, ax_wp = pl.subplots(**fig_params)
color =  np.zeros((len(packets[0]), 4))
color[:, 2] = 1
color[:, 3] = packets[2]/max(packets[2])
ax_wp.scatter(packets[0], np.zeros(len(packets[0])),
              s=2000*packets[1], c=color)
ax_wp.yaxis.set_ticks([])
ax_wp.scatter([0, ion_position], [0, 0], s=100, c='r')
ax_wp.set_xlabel(r"$x, \AA$")

fig_state.savefig("pics/packets_scheme.%s" % args.save_format,
                  bbox_inches='tight', dpi=400)
