#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
sys.path.append("/home/kna/mipt/mol_mod/mdapp_analyser")

import pylab as pl
import numpy as np
import argparse
import itertools
import matplotlib as mpl
from serie import Serie, sf, getPar
from builder import System, Particle
from itertools import imap

coul_pref = 14.39965172693122; # e^2/(4*pi*eps0), eV*A
EH1 = -13.6056980651225   #eV
EH2 = 4.52                #eV
EH2_SCF = 3.6364          #eV http://rmp.aps.org/abstract/RMP/v32/i2/p219_1

parser = argparse.ArgumentParser(description='Anaslysis H2 malecules '
                                 'calculation by GRidMD')
parser.add_argument('filename', type=str, nargs=1,
                    help='summary.dat to process')
parser.add_argument('-s', '--packet-number', type=int, nargs="*",
                   help='Use only data series with specified number of packets')
parser.add_argument('-t', '--totals', action="store_true",
                    help='Plot bond energy and length plots')
parser.add_argument('-f', '--full', action="store_true",
                    help="Don't subtract ground state energy for athoms")
parser.add_argument('-i', '--ion_none', action="store_true",
                    help="Don't subtract ion interaction energy")
parser.add_argument('-p', '--precise_baseline', action="store_true",
                    help="Use precise baseline for bond energy calculation")
parser.add_argument('-d', '--dimensions', type=float, nargs='*',
                    help="figure dimension, inches")
parser.add_argument('-w', '--wave-packets', action="store_true",
                    help="draw wavepackets")
parser.add_argument('--save-format', action='store', type=str,
                    default='pdf', help='save file format')

args = parser.parse_args()

data = dict()
summary_file = open(args.filename[0])
for line in summary_file:
    param_line = line.split()
    s, l = map(lambda mo: mo.group(), getPar(line, ("s", "l")))
    s = int(s.strip('s'))
    if args.packet_number is None or s in args.packet_number:
        l = float(l)
        if s not in data.keys():
            data[s] = {
                "E": list(),
                "l": list(),
                "packets": list()
            }

        E = float(param_line[1])
        if any(imap(lambda x: x != '0', param_line[2:])):
            print(line.strip())
        if not args.ion_none:
            E += coul_pref/l
        xml_file = param_line[0].replace("H2/", "results/").replace(".xml", "_opt.xml", 1)
        system = System(xml_file)

        electron = filter(lambda particle: particle.type=='e', system.particles.itervalues())


        try:
            i = data[s]["l"].index(l)
            data[s]["E"][i] = E
            data[s]["packets"][i] = (np.array(map(lambda packet: (packet.x, packet.width, packet.c1),
                                                  electron[0].packets+electron[1].packets)))/l
        except ValueError:
            data[s]["E"].append(E)
            data[s]["l"].append(l)
            data[s]["packets"].append((np.array(map(lambda packet: (packet.x, packet.width, packet.c1),
                                                    electron[0].packets+electron[1].packets)))/l)

        # format:
        # [l_number][packet_number][parameter(x, width, c)]

summary_file.close()
all_colors = np.linspace(0, 1, num=len(data.keys()))

if not args.full:
    summary_h1_file = open("results_h1/summary.dat")
    for line in summary_h1_file:
        s = getPar(line, ("s", ))[0].group()
        s = int(s.strip('s'))
        E = float(line.split()[1])
        if s in data.keys():
            data[s]["EH1"] = E
    summary_h1_file.close()

fig_params = dict()
if args.dimensions is not None:
    if len(args.dimensions) != 2:
        print("You maust specify both dimensions")
        sys.exit(1)
    fig_params['figsize'] = args.dimensions

fig_El, ax_El = pl.subplots(**fig_params)
pl.grid()
if args.wave_packets:
    ax_packets = ax_El.twinx()
color_getter = itertools.cycle(all_colors)
lines = list()
for s, el_data in data.iteritems():
    el_data["l"] = np.array(el_data["l"])
    el_data_order = el_data['l'].argsort()
    el_data["E"] = np.array(el_data["E"])[el_data_order]
    c_max = np.max(np.transpose(el_data["packets"], axes=(2, 0, 1))[2], axis=1)
    el_data["packets"] = np.transpose(el_data["packets"], axes=(1, 2, 0))
    el_data["l"].sort()
    color_num = color_getter.next()
    color = mpl.cm.jet(color_num)
    C = np.ones((len(el_data['packets']), len(el_data['l'])))*color_num
    colors = mpl.cm.jet(C)


    label = ""
    if not args.full:
        if "EH1" in el_data and not args.precise_baseline:
            el_data["E"] -= 2*el_data["EH1"]
        else:
            el_data["E"] -= 2*EH1
            #            label = "Precise baseline used, "

    if args.wave_packets:
        for l in el_data['l']:
            ax_packets.axvline(l, linestyle="--", c='black')
        colors_arrays = itertools.cycle(colors)
        for packet in el_data["packets"]:
            colors_per_packet = colors_arrays.next()
            colors_per_packet[:, 3] = packet[2]/c_max
            ax_packets.scatter(el_data["l"], packet[0], c=colors_per_packet,
                               s=100*packet[1]/el_data['l'])
    ax_El.plot(el_data["l"], el_data["E"], markersize=15,
               label=label + ur"%d пакет(а)(ов)" % s, c=color)

ax_El.set_xlabel(ur"Длина связи, $\AA$")
ax_El.set_ylabel(ur"Энергия связи, эВ")
if args.wave_packets:
    ax_packets.set_ylabel("Packet coordinate, relative to bond length, 1")

ax_El.set_xlim((0.5, 1))
ax_El.set_ylim((-4, 0))
ax_El.legend(loc='center left', bbox_to_anchor=(1, 0.5))
fig_El.savefig("pics/El.%s" % args.save_format,
               bbox_inches='tight')

if args.totals:
    Es = list()
    ls = list()
    ss = list()
    l_err = 0.02
    for s, vals in data.iteritems():
        Es.append(min(vals['E']))
        ss.append(s)
        ls.append(vals['l'][vals['E'].argmin()])

    figE, axE = pl.subplots(**fig_params)
    axE.axhline(-EH2, label = "Exact energy", zorder=1)
    axE.axhline(-EH2_SCF, c="magenta", label = "Hartree-Fock limit",
                zorder=1)
    axE.scatter(ss, Es, s=30, zorder=10)
    legend_artist = axE.legend(loc='upper left', bbox_to_anchor=(-0.1,-0.15))
    axE.set_xlim(0)
    labels = list()
    labels.append(axE.set_xlabel("Num. of packets, 1"))
    labels.append(axE.set_ylabel("Bond energy, eV"))
    labels.append(legend_artist)
    pl.grid()
    figE.savefig("pics/BondE_l.%s" % args.save_format,
                 bbox_extra_artists=labels,
                 bbox_inches='tight', dpi=400)

    figL, axL = pl.subplots(**fig_params)
    axL.errorbar(ss, ls, yerr=l_err, fmt="o")
    axL.axhline(0.74)
    axL.set_xlabel("Num. of packets, 1")
    axL.set_ylabel(r"Bond length, $\AA$")
    pl.grid()
    figL.savefig("pics/sl.%s" % args.save_format,
                 bbox_inches='tight', dpi=400)
