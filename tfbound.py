from __future__ import division
import numpy as np
import pylab as pl
import sys
sys.path.append("/home/kna/mipt/mol_mod/mdapp_analyser")
from numpy import sqrt, log
import re
from serie import Serie, sf
import serie
import random

def lw_gen():
    i = 7
    while True:
        while i > 3:
            i -= 2
            yield i
        yield 1


id = random.random()
data = map(lambda x: Serie(x, get_totals = True), filter(lambda x: x.find('.cfg')>0, sys.argv[1:]))

tfbound = pl.figure()
lw_g = lw_gen()
for cserie in data:
    pl.plot(cserie.totals[1], cserie.totals[5], lw=lw_g.next(),
            label=cserie.config['Charm']['Legend']+" t_max = %.2f"%cserie.t[-1])

pl.xlabel("t")
pl.ylabel("fbound")
pl.legend()
sf("tfbound_%f.png"%id, tfbound)
